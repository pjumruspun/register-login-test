package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/go-redis/redis"
)

func IsEmpty(data string) bool {
	if len(data) == 0 {
		return true
	} else {
		return false
	}
}

type User struct {
	Password string `json:"password"`
	Money    int    `json:"money"`
}

func main() {
	defaultMoney := 100
	client := redis.NewClient(&redis.Options{
		Addr:     "redis:6379",
		Password: "",
		DB:       0,
	})

	username, password := "", ""
	mux := http.NewServeMux()

	mux.HandleFunc("/register", func(w http.ResponseWriter, r *http.Request) {
		r.ParseForm()

		// Available for testing.
		for key, value := range r.Form {
			fmt.Printf("%s = %s\n", key, value)
		}

		username = r.FormValue("username") // Data from the form
		password = r.FormValue("password") // Data from the form

		// Empty data checking
		uNameCheck := IsEmpty(username)
		pwdCheck := IsEmpty(password)

		if uNameCheck || pwdCheck {
			fmt.Fprintf(w, "ErrorCode is -10 : There is empty data.")
			return
		}

		err := client.Get(username).Err()
		if err != nil {
			fmt.Println(err)
		}
		if err != redis.Nil {
			fmt.Println("Key exist")
			fmt.Fprintf(w, "user already exists")
			return
		}

		newUser := User{Password: password, Money: defaultMoney}

		jsonData, err := json.Marshal(&newUser)
		err = client.Set(username, jsonData, 0).Err()
		rawData, err := client.Get(username).Result()
		outData := []byte(rawData)
		outUser := User{}

		err = json.Unmarshal(outData, &outUser)

		fmt.Print("outUser.Money: ")
		fmt.Println(outUser.Money)
		fmt.Print("outUser.Password: ")
		fmt.Println(outUser.Password)
		fmt.Fprintf(w, "Register successful!")
	})

	mux.HandleFunc("/login", func(w http.ResponseWriter, r *http.Request) {
		r.ParseForm()

		username = r.FormValue("username") // Data from the form
		password = r.FormValue("password") // Data from the form

		// Empty data checking
		uNameCheck := IsEmpty(username)
		pwdCheck := IsEmpty(password)

		if uNameCheck || pwdCheck {
			fmt.Fprintf(w, "ErrorCode is -10 : There is empty data.")
			return
		}

		rawData, err := client.Get(username).Result()
		if err != nil {
			fmt.Println(err)
		}

		outData := []byte(rawData)
		outUser := User{}

		err = json.Unmarshal(outData, &outUser)

		dbPwd := outUser.Password

		if dbPwd == password {
			fmt.Fprintln(w, "ok")
		} else {
			fmt.Fprintln(w, "unauthorized")
		}
	})

	mux.HandleFunc("/setmoney", func(w http.ResponseWriter, r *http.Request) {
		r.ParseForm()

		username = r.FormValue("username")                         // Data from the form
		moneyvalue, err := strconv.Atoi(r.FormValue("moneyvalue")) // Data from the form

		uNameCheck := IsEmpty(username)

		if err != nil {
			fmt.Println(err)
			fmt.Println("MONEY INVALID SYNTAX")
			return
		}

		if moneyvalue < 0 {
			fmt.Println(w, "ERROR: Money cannot be negative value")
		}

		if uNameCheck {
			fmt.Fprintf(w, "ErrorCode is -10 : There is empty data.")
			return
		}

		value, err := client.Get(username).Result()
		if err == redis.Nil {
			fmt.Println("Key not exist")
			fmt.Fprintf(w, "user not exist")
			return
		}

		outDataFromDB := []byte(value) // Trying to apply old password
		outUserFromDB := User{}

		err = json.Unmarshal(outDataFromDB, &outUserFromDB)

		newUser := User{Password: outUserFromDB.Password, Money: moneyvalue} // Apply old password but new money

		jsonData, err := json.Marshal(&newUser)
		err = client.Set(username, jsonData, 0).Err()
		val3, err := client.Get(username).Result()

		outData := []byte(val3)
		outUser := User{}

		err = json.Unmarshal(outData, &outUser)

		fmt.Print("outUser.Money: ")
		fmt.Println(outUser.Money)
		fmt.Print("outUser.Password: ")
		fmt.Println(outUser.Password)
		fmt.Fprintf(w, "Set Money Successful!")
	})

	mux.HandleFunc("/getmoney", func(w http.ResponseWriter, r *http.Request) {
		r.ParseForm()

		username = r.FormValue("username") // Data from the form
		uNameCheck := IsEmpty(username)

		if uNameCheck {
			fmt.Fprintf(w, "ErrorCode is -10 : There is empty data.")
			return
		}

		value, err := client.Get(username).Result()
		if err == redis.Nil {
			fmt.Println("Key not exist")
			fmt.Fprintf(w, "user not exist")
			return
		}

		outDataFromDB := []byte(value) // Trying to apply old password
		outUserFromDB := User{}

		err = json.Unmarshal(outDataFromDB, &outUserFromDB)

		fmt.Print("outUser.Money: ")
		fmt.Println(outUserFromDB.Money)
		fmt.Print("outUser.Password: ")
		fmt.Println(outUserFromDB.Password)
		fmt.Fprintf(w, strconv.Itoa(outUserFromDB.Money))
	})

	http.ListenAndServe(":8080", mux)
}
